﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HigleySequence
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(Enumerable.Range(1, 100000).Select(n => n.Higley().Count).Sum());
            Console.ReadKey();
        }

    }
}
