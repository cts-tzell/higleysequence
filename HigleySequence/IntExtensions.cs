﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HigleySequence
{
    static class IntExtensions
    {
        public static IList<int> PrimeFactors(this int @this)
        {
            var primes = new List<int>();

            for (int div = 2; div <= @this; div++)
            {
                while (@this % div == 0)
                {
                    primes.Add(div);
                    @this = @this / div;
                }
            }

            return primes;
        }

        private static IList<int> Higley(IList<int> numbers)
        {
            var number = numbers.Last();
            var factors = number.PrimeFactors();
            var higley = factors.Sum() * factors.Count;
            if (numbers.Contains(higley)) return numbers;
            numbers.Add(higley);
            return Higley(numbers);
        }

        public static IList<int> Higley(this int @this)
        {
            return Higley(new List<int> { @this });
        }
    }
}
